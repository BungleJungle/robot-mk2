/*
 * shared_hardware.h
 *
 *  Created on: Jan 7, 2025
 *      Author: Steven
 */

#ifndef BSP_INC_SHARED_HARDWARE_H_
#define BSP_INC_SHARED_HARDWARE_H_

#include "stdint.h"
#include "types.h"
#include "gpio.h"
#include "main.h"
#include "system_state.h"

static inline void TurnOffGreenLed()
{
	signalController.blinkGreenLED = 0;
	HAL_GPIO_WritePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin, 0);
}

static inline void TurnOffRedLed()
{
	signalController.blinkRedLED = 0;
	HAL_GPIO_WritePin(RED_LED_GPIO_Port, RED_LED_Pin, 0);
}

static inline void TurnOnGreenLed()
{
	signalController.blinkGreenLED = 1;
}

static inline void TurnOnRedLed()
{
	signalController.blinkRedLED = 1;
}


void SH_Init();
void SH_GreenLEDToggle();
void SH_RedLEDToggle();
u32 SH_GetRandomInt();
u32 SH_GetRandomIntClamp(uint32_t max, uint32_t min);
float SH_GetRandomFloat();

u32 SH_GeneralTimer();

#endif /* BSP_INC_SHARED_HARDWARE_H_ */
