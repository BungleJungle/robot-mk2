/*
 * bsp_adc.h
 *
 *  Created on: Dec 22, 2024
 *      Author: Steven
 */
#ifndef ADC_MANAGER_H
#define ADC_MANAGER_H

#include "stdint.h"
#include "main.h"

enum
{
	ADC_M0_POSITION,
	ADC_M1_POSITION,
	ADC_M2_POSITION,
	ADC_M3_POSITION,

	ADC_NUM_CHANNELS
};

#define POSITION_BUFFER_SIZE	8

extern const volatile uint16_t adcBuffer[ADC_NUM_CHANNELS] __attribute__((aligned(CACHE_LINE_SIZE)));

void BSP_AdcInit();
void BSP_AdcUpdate();
void BSP_StartSampling();
void BSP_StopSampling();

#endif
