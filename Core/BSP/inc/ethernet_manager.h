/*
 * ethernet.h
 *
 *  Created on: Dec 23, 2024
 *      Author: Steven
 */

#ifndef BSP_INC_ETHERNET_MANAGER_H_
#define BSP_INC_ETHERNET_MANAGER_H_

#include "types.h"

ReturnType ETH_Init();
ReturnType ETH_Update();

#endif /* BSP_INC_ETHERNET_MANAGER_H_ */
