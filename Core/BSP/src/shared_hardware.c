/*
 * shared_hardware.c
 *
 *  Created on: Jan 7, 2025
 *      Author: Steven
 */

#include "shared_hardware.h"
#include "main.h"
#include "rng.h"
#include "tim.h"

void SH_Init()
{
	HAL_TIM_Base_Start_IT(&htim23);

	return;
}

void SH_GreenLEDToggle()
{
	HAL_GPIO_TogglePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin);
}

void SH_RedLEDToggle()
{
	HAL_GPIO_TogglePin(RED_LED_GPIO_Port, RED_LED_Pin);
}

u32 SH_GetRandomInt()
{
	u32 randInt;

	HAL_RNG_GenerateRandomNumber(&hrng, &randInt);

	return randInt;
}

u32 SH_GetRandomIntClamp(uint32_t max, uint32_t min)
{
	u32 randInt;

	HAL_RNG_GenerateRandomNumber(&hrng, &randInt);

	randInt = randInt % max;

	return randInt;
}

float SH_GetRandomFloat()
{

}

u32 SH_GeneralTimer()
{
	return htim23.Instance->CNT;
}


