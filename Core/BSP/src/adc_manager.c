/*
 * bsp_adc.c
 *
 *  Created on: Dec 22, 2024
 *      Author: Steven
 */

#include "adc_manager.h"
#include "adc.h"
#include "tim.h"

const volatile uint16_t adcBuffer[ADC_NUM_CHANNELS] __attribute__((aligned(CACHE_LINE_SIZE)));

static uint16_t m0Index = 0;

void BSP_AdcInit()
{
	//
	//MX_ADC1_Init();

	osDelay(15);

	// ADC single ended calibration
	if (HAL_ADCEx_Calibration_Start(&hadc1, ADC_CALIB_OFFSET, ADC_SINGLE_ENDED) != HAL_OK)
	{
		return;
	}

	osDelay(15);
	//SCB_InvalidateDCache_by_Addr(adcBuffer, ADC_NUM_CHANNELS);

	if (HAL_ADC_Start_DMA(&hadc1, (uint32_t*)adcBuffer, ADC_NUM_CHANNELS) != HAL_OK)
	{
		return;
	}

	osDelay(5);

	return;
}

void BSP_AdcUpdate(uint16_t* newValues)
{
	SCB_CleanInvalidateDCache_by_Addr(adcBuffer, ADC_NUM_CHANNELS);

	newValues[0] = adcBuffer[0];
	newValues[1] = adcBuffer[2];
	newValues[2] = adcBuffer[1];
	newValues[3] = adcBuffer[4];

	return;
}

void BSP_StartSampling()
{
	HAL_TIM_Base_Start_IT(&htim16);
}

void BSP_StopSampling()
{
	HAL_TIM_Base_Stop_IT(&htim16);
}



