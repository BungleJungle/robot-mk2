/*
 * kinematics.c
 *
 *  Created on: Jan 1, 2025
 *      Author: Steven
 */


#include "kinematics.h"
#include "matrix_math.h"
#include "stdio.h"
#include "math.h"
#include "main.h"

#define PI 			3.141592654f
#define RAD2DEG		(180.0f / PI)

DHParams dhTable[NUMBER_OF_FRAMES];
fm4 	 endEffector;

static void populateDH(DHParams* dhTable);

void KM_Init()
{
	populateDH(dhTable);
}

void KM_KinematicsCyclic()
{
	//KM_ForwardKinematics();
}

void KM_ForwardKinematics()
{
	fm4 tempMat;
	m4_zero(&tempMat);
	//for (int motorId = 0; motorId < MOTOR_INDEX_COUNT; ++motorId)
	// currently only 3 joints are assembled
	for (int motorId = 0; motorId < 3; ++motorId)
	{
		m4_rot(&tempMat,
			dhTable[motorId].t,
			dhTable[motorId].a,
			dhTable[motorId].d,
			dhTable[motorId].r);

		m4_mul(&endEffector, &tempMat, &endEffector);
	}

	return;
}

void KM_InverseKinematics(DHParams* dhTable, fm4* dst)
{
	const float x = dst->m[0][3];
	const float y = dst->m[1][3];
	const float z = dst->m[2][3];

	const float a1 = dhTable[MOTOR_0].d;
	const float a2 = dhTable[MOTOR_1].r;
	const float a3 = dhTable[MOTOR_2].r;

	float r1 	= sqrt(x*x + y*y + (z-a1)*(z-a1));
	float temp1 = (z-a1) / sqrt(x*x + y*y);
	float temp2 = (a3*a3 - a2*a2 - r1*r1) / (-2*a2*r1);

	float a = atan(y/x) * RAD2DEG + 90.0f;
	float b = 180.0f - (atan(temp1) + acos(temp2)) * RAD2DEG;
	float c = 180.0f - acos( (r1*r1 - a3*a3 - a2*a2) / (-2*a3*a2) ) * RAD2DEG;
}

static void populateDH(DHParams* dhTable)
{
	/*
	 *		DH table params
	 *
	 *		Operations are done in the following order
	 *		d = displacement along the z-axis	| units in mm
	 *		t = rotation about the z-axis		| units in degrees
	 *		r = displacement along the x-axis	| uints in mm
	 *		a = rotation about the x-axis		| uints in degrees
	 */

	dhTable[MOTOR_0].d = 120.3f;
	//dhTable[STEPPER_M0].t = motorPositions[STEPPER_M0];
	dhTable[MOTOR_0].r = 0.0f;
	dhTable[MOTOR_0].a = 0.0f;

	dhTable[MOTOR_1].d = 38.65f;
	//dhTable[STEPPER_M1].t = motorPositions[STEPPER_M1] - 90.0f;
	dhTable[MOTOR_1].r = 52.0f;
	dhTable[MOTOR_1].a = -90.0f;

	dhTable[MOTOR_2].d = 0.0f;
	//dhTable[STEPPER_M2].t = motorPositions[STEPPER_M2];
	dhTable[MOTOR_2].r = 355.6f;
	dhTable[MOTOR_2].a = 0.0f;

	dhTable[MOTOR_3].d = 0.0f;
	//dhTable[STEPPER_M3].t = motorPositions[STEPPER_M3] - 90.0f;
	dhTable[MOTOR_3].r = 185.738f;
	dhTable[MOTOR_3].a = 0;

	// needs to be updated
	dhTable[MOTOR_4].d = 0;
	//dhTable[SERVO_M4].t = motorPositions[SERVO_M4];
	dhTable[MOTOR_4].r = 220;
	dhTable[MOTOR_4].a = 0;

	// needs to be updated
	dhTable[MOTOR_5].d = 0;
	//dhTable[SERVO_M5].t = motorPositions[SERVO_M5];
	dhTable[MOTOR_5].r = 220;
	dhTable[MOTOR_5].a = 0;
}
