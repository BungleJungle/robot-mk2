/*
 * system_state.c
 *
 *  Created on: Dec 23, 2024
 *      Author: Steven
 */

#include "system_state.h"
#include "motor_controller.h"
#include "lwip.h"
#include "ethernet_manager.h"
#include "adc_manager.h"
#include "shared_hardware.h"

#define SECOND_DELAY 1000

SignalController signalController;

static void ProcessFaults();

ReturnType ST_ApplcationInit()
{
//	MX_LWIP_Init();

	ETH_Init();

	BSP_AdcInit();

	ST_Init();

	MTR_Init();

	SH_Init();

	return OK;
}

ReturnType ST_Init()
{
	ReturnType status = OK;

	signalController.systemState = STATE_INIT;
	signalController.initalPowerUp = 1;

	TurnOffRedLed();
	TurnOnGreenLed();

	return status;
}

ReturnType ST_Cyclic()
{
	ProcessFaults();

	switch (signalController.systemState)
	{
		case STATE_INIT:
		{
			// do system check
			ST_ApplcationInit();

			MTR_MoveMotor(0, 90.0f, 10.0f);
			MTR_MoveMotor(1, 135.0f, 5.0f);
			MTR_MoveMotor(2, 0.0f, 5.0f);

			// move to homing state
			signalController.systemState = STATE_HOMING;

			MTR_StartPulse();

			// go to home position


			break;
		}
		case STATE_IDLE:
		{

			break;
		}
		case STATE_HOMING:
		{
			//
			uint8_t motorState = MTR_GetMotorStates();

			if ((motorState & 0x1) != 0x1)
			{
				float randomFloat = (float)SH_GetRandomIntClamp(240, 5);
				MTR_MoveMotor(0, randomFloat, 5.0f);
				MTR_EndPulse();
				signalController.systemState = STATE_REST;
			}

			break;
		} 
		case STATE_PLANNING:
		{

			break;
		} 
		case STATE_MOVING:
		{

			break;
		} 
		case STATE_AT_TARGET:
		{

			break;
		}
		case STATE_RETURNING:
		{

			break;
		} 
		case STATE_REST:
		{
			uint32_t cycleTicks = osKernelGetTickCount();
			osDelayUntil(cycleTicks + SECOND_DELAY);
			signalController.systemState = STATE_HOMING;
			MTR_StartPulse();
			break;
		}
		case STATE_FAULTED:
		{
			MTR_EndPulse();
			TurnOffGreenLed();
			TurnOnRedLed();
			break;
		}
		default:
		{

			break;
		}
	}

	return OK;
}

static void ProcessFaults()
{
	if (signalController.faultIndexer)
	{
		signalController.systemState = STATE_FAULTED;
	}
}





