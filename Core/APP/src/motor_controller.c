/*
 * motor_controller.c
 *
 *  Created on: Dec 23, 2024
 *      Author: Steven
 */

#include "motor_controller.h"
#include "adc_manager.h"
#include "digital_filter.h"
#include "system_state.h"
#include "shared_hardware.h"
#include "main.h"
#include "tim.h"
#include "gpio.h"
#include "types.h"
#include "string.h"
#include "cmsis_os.h"
#include "stdbool.h"

///@ part number 6127V1A360L5FS
// voltage range of 0.2V to 4.8V
#define SENSOR_V_START 	3972
#define SENSOR_V_STOP	95323

#define MOTOR_ROTATE_CW		1
#define MOTOR_ROTATE_CCW	0
#define MOTOR_ON 			1
#define MOTOR_OFF			0

#define STEPPER_LENGTH 		1.8f

#define NUMBER_OF_MOTORS	5
#define PULSE_DURATION		1 	// in uS
#define MTR_UDATE_FREQUENCY	20000

static inline void StepperModulation(MotorContext* m_ctx) __attribute__((always_inline));
static inline void CheckPosition(MotorContext* m_ctx) __attribute__((always_inline));
static void GetInitalCondition();
static void MotorOverrunProtection(MotorContext* m_ctx);
static void MotorOverspinProtection(MotorContext* m_ctx);
static void UpdateMotorPID(MotorContext* mCtx);
static inline uint16_t clamp_s(uint16_t value,  uint16_t min, uint16_t max);
static inline void ResetPin(MotorContext* m_ctx);

BxFilterContext BxM0 __attribute__((aligned(CACHE_LINE_SIZE)));
BxFilterContext BxM1 __attribute__((aligned(CACHE_LINE_SIZE)));
BxFilterContext BxM2 __attribute__((aligned(CACHE_LINE_SIZE)));
BxFilterContext BxM3 __attribute__((aligned(CACHE_LINE_SIZE)));
BxFilterContext BxM4 __attribute__((aligned(CACHE_LINE_SIZE)));
BxFilterContext BxM5 __attribute__((aligned(CACHE_LINE_SIZE)));

static MotorContext motors[NUMBER_OF_MOTORS] __attribute__((aligned(CACHE_LINE_SIZE)));
static u32 currentRunTimer = 0;
MotorContext* const m0 = &motors[0];
MotorContext* const m1 = &motors[1];
MotorContext* const m2 = &motors[2];
MotorContext* const m3 = &motors[3];
MotorContext* const m4 = &motors[4];
MotorContext* const m5 = &motors[5];

ReturnType MTR_Init()
{
	ReturnType status = OK;

	memset(m0, 0, sizeof(MotorContext));
	memset(m1, 0, sizeof(MotorContext));
	memset(m2, 0, sizeof(MotorContext));
	memset(m3, 0, sizeof(MotorContext));

	DF_BoxcarInit(&BxM0);
	DF_BoxcarInit(&BxM1);
	DF_BoxcarInit(&BxM2);
	DF_BoxcarInit(&BxM3);
	DF_BoxcarInit(&BxM4);
	DF_BoxcarInit(&BxM5);

	m0->DIR_GPIOx 			= GPIOD;
	m0->DIR_GPIO_PIN 		= MOTOR_0_DIRECTION_Pin;
	m0->PULSE_GPIOx 		= GPIOD;
	m0->PULSE_GPIO_PIN 		= GPIO_PIN_0;
	m0->maxRuntime			= MTR_UDATE_FREQUENCY * 15;	// max runtime of 15 seconds
	m0->stepSize 			= STEPPER_LENGTH / 19.0f;
	m0->maxSpeed			= 15;
	m0->minSpeed			= 90 << 4;
	m0->maxPosition			= 270 << 4;
	m0->positionZeroOffset 	= 4000;
	m0->positionLimit 		= 65000;
	m0->positionEpsilon 	= 16;
	m0->homePosition		= 180 << 4;
	m0->MOTOR_REVERSAL		= 0;

	m1->DIR_GPIOx 			= MOTOR_1_DIRECTION_GPIO_Port;
	m1->DIR_GPIO_PIN 		= MOTOR_1_DIRECTION_Pin;
	m1->PULSE_GPIOx 		= MOTOR_1_PULSE_GPIO_Port;
	m1->PULSE_GPIO_PIN 		= MOTOR_1_PULSE_Pin;
	m1->stepSize 			= STEPPER_LENGTH / 19.0f;
	m1->maxSpeed			= 50;
	m1->minSpeed			= 90 << 4;
	m1->maxPosition			= 180 << 4;
	m1->positionZeroOffset 	= 5560;
	m1->positionLimit 		= 49647;
	m1->positionEpsilon 	= 16;
	m1->homePosition		= 90 << 4;
	m1->MOTOR_REVERSAL		= 1;

	m2->DIR_GPIOx 			= MOTOR_2_DIRECTION_GPIO_Port;
	m2->DIR_GPIO_PIN 		= MOTOR_2_DIRECTION_Pin;
	m2->PULSE_GPIOx 		= MOTOR_2_PULSE_GPIO_Port;
	m2->PULSE_GPIO_PIN 		= MOTOR_2_PULSE_Pin;
	m2->stepSize 			= STEPPER_LENGTH / 20.0f;
	m2->maxSpeed			= 25;
	m2->minSpeed			= 90 << 4;
	m2->maxPosition			= 0 << 4;
	m2->positionZeroOffset 	= 26809;
	m2->positionLimit 		= 64000;
	m2->positionEpsilon 	= 2 << 4;
	m2->homePosition		= 0 << 4;
	m2->MOTOR_REVERSAL		= 1;

	m3->DIR_GPIOx 			= MOTOR_3_DIRECTION_GPIO_Port;
	m3->DIR_GPIO_PIN 		= MOTOR_3_DIRECTION_Pin;
	m3->PULSE_GPIOx 		= MOTOR_3_PULSE_GPIO_Port;
	m3->PULSE_GPIO_PIN 		= MOTOR_3_PULSE_Pin;
	m3->stepSize 			= STEPPER_LENGTH / 19.0f;
	m3->maxSpeed			= 25;
	m3->minSpeed			= 45 << 4;
	m3->maxPosition			= 270 << 4;
	m3->positionZeroOffset 	= 5958;
	m3->positionLimit 		= 64000;
	m3->positionEpsilon 	= 2 << 4;
	m3->homePosition		= 135 << 4;
	m3->MOTOR_REVERSAL		= 0;

	// init motor position, yes I know its only supposed to be used in the ISR but it avoid more code

	osDelay(50);

	GetInitalCondition();

	osDelay(50);

	signalController.MTR_INIT = 1;

	return status;
}

void MTR_CheckPositionISR()
{
	u32 m0pos = 0, m1pos = 0, m2pos = 0, m3pos = 0;
	u16 newInputs[4];

	BSP_AdcUpdate(newInputs);

	m0pos = DF_BoxcarFilter(&BxM0, newInputs[0]);
	m1pos = DF_BoxcarFilter(&BxM1, newInputs[1]);
	m2pos = DF_BoxcarFilter(&BxM2, newInputs[2]);
	m3pos = DF_BoxcarFilter(&BxM3, newInputs[3]);

	m0->currentPosition = ((m0pos - m0->positionZeroOffset) >> 4);
	m1->currentPosition = ((m1pos - m1->positionZeroOffset) >> 4);
	m2->currentPosition = ((m2pos - m2->positionZeroOffset) >> 4);
	m3->currentPosition = ((m3pos - m3->positionZeroOffset) >> 4);

	// TODO add more motors
	for (int i = 0; i < 4; ++i)
	{
		CheckPosition(&motors[i]);
		MotorOverspinProtection(&motors[i]);
	}
}

void MTR_PulseISR()
{
	currentRunTimer += 1;

	StepperModulation(m0);
	StepperModulation(m1);
	StepperModulation(m2);
	StepperModulation(m3);
}

void MTR_StartPulse()
{
	// start collecting samples when robot is moving
	BSP_StartSampling();

	HAL_TIM_Base_Start_IT(&htim13);

	currentRunTimer = 0;
}

void MTR_EndPulse()
{
	// start collecting samples when robot is moving
	BSP_StopSampling();

	HAL_TIM_Base_Stop_IT(&htim13);

	currentRunTimer = 0;
}

// motor ID in unit
// position in degrees
// time in seconds
void MTR_MoveMotor(uint8_t motor_id, float position, float time)
{
	MotorContext* const m_ptr = &motors[motor_id];
	const float maxPosition = ((float)m_ptr->maxPosition / 16.0f);

	if (position > maxPosition)
	{
		position = maxPosition;
		ADD_WARNING(W_INVALID_POSITION_REQUEST);
	}
	else if (position < 0.0f)
	{
		position = 0.0f;
		ADD_WARNING(W_INVALID_POSITION_REQUEST);
	}

	m_ptr->targetPosition = ((AngleUnit)position) << 4;
	
	// Number of pulses to target in float form, muliply by 16 to account for 1/16 units
	float fltDistance = ((position*16.0f) - (float)m_ptr->currentPosition) / (m_ptr->stepSize);

	// convert to distance per millisecond, to reduce floating point errors
	// time between pulses
	float linearPulseTimer = ( fltDistance / time);

	// get the direction bit based on sign bit
	m_ptr->MOTOR_DIRECTION = ((*(uint32_t*)&linearPulseTimer) & 0x8000) >> 31;

	// ABS(linear pulse timer)
	linearPulseTimer = abs(linearPulseTimer);

	// convert to number pulse frequency, 50uS update frequency
	m_ptr->PULSE_FREQ = (u16)((time*1e5) / linearPulseTimer );

	// check for pulse timing is within bounds
	if (m_ptr->PULSE_FREQ > (m_ptr->minSpeed)
		|| m_ptr->PULSE_FREQ < (m_ptr->maxSpeed))
	{
		// add a warning to the signal controller
		ADD_WARNING(W_INVALID_SPEED_REQUEST);
		m_ptr->PULSE_FREQ = clamp_s(m_ptr->PULSE_FREQ, m_ptr->maxSpeed, m_ptr->minSpeed);
	}

	// avoid a possible motion jitter by checking position
	CheckPosition(m_ptr);

	ResetPin(m_ptr);

	return;
}

void MTR_MoveMotorNonLinear(uint8_t motor_id, float position, float time)
{
	int steps = 20;			// step size will increase smoothness of arm movement, but at a cost of accuracry
	float ipoint = 0;
	float ci[4];
	float t;

	MotorContext* const m_ptr = &motors[motor_id];
	const float max_position = ((float)m_ptr->maxPosition / 16.0f);
	const float f_current_position = ((float)m_ptr->currentPosition / 16.0f);
	const float d_step = time / (float)m_ptr->polynomialStepCount;

	if (motor_id > NUMBER_OF_MOTORS)
	{
		ADD_WARNING(W_INVALID_CHANNEL_REQUEST);
	}

	if (position > max_position)
	{
		position = max_position;
		ADD_WARNING(W_INVALID_POSITION_REQUEST);
	}
	else if (position < 0.0f)
	{
		position = 0.0f;
		ADD_WARNING(W_INVALID_POSITION_REQUEST);
	}

	uint16_t temp[10];

	temp[0] = 0;
	temp[1] = (int)(f_current_position/max_position);

	for (int j = 0; j < steps; ++j)
	{
		//t = j * dstep;

		//m_ptr->coefficents[0] = svcntl->sv_state[i].position * svcntl->sv_state[i].range;
		//m_ptr->coefficents[1] = 0;
		//m_ptr->coefficents[2] = 3 * (arm->theta[i] - svcntl->sv_state[i].position * svcntl->sv_state[i].range) / (dt * dt);
		//m_ptr->coefficents[3] = 2 * ((svcntl->sv_state[i].position * svcntl->sv_state[i].range) - arm->theta[i]) / (dt * dt * dt);

		//printf("cofactors = %f, %f, %f, %f \n", ci[0], ci[1], ci[2], ci[3]);
		//ipoint = (ci[0] + ci[1] * t + ci[2] * t * t + ci[3] * t * t * t) / svcntl->sv_state[i].range;
	}

	if (m_ptr->PULSE_FREQ > (m_ptr->minSpeed)
		|| m_ptr->PULSE_FREQ < (m_ptr->maxSpeed))
	{
		// add a warning to the signal controller
		ADD_WARNING(W_INVALID_SPEED_REQUEST);
		m_ptr->PULSE_FREQ = clamp_s(m_ptr->PULSE_FREQ, m_ptr->maxSpeed, m_ptr->minSpeed);
	}

	// clear controller intergral term
	ResetPin(m_ptr);

	return;
}

void MTR_Cylcic()
{

	return;
}

static inline void StepperModulation(MotorContext* m_ctx)
{
	if (m_ctx->MOTOR_ENABLE > 0)
	{
		m_ctx->PULSE_TIMER += 1;

		if (m_ctx->PULSE_TIMER >= m_ctx->PULSE_FREQ)
		{
			m_ctx->PULSE_OUTPUT = GPIO_PIN_SET;

			if (m_ctx->PULSE_TIMER >= (m_ctx->PULSE_FREQ + PULSE_DURATION))
			{
				m_ctx->PULSE_OUTPUT = GPIO_PIN_RESET;
				m_ctx->PULSE_COUNTER += 1;
				m_ctx->PULSE_TIMER = 0;
			}
		}

		HAL_GPIO_WritePin(m_ctx->PULSE_GPIOx, m_ctx->PULSE_GPIO_PIN, m_ctx->PULSE_OUTPUT);
	}
}

static inline void CheckPosition(MotorContext* m_ctx)
{
	u8 high = m_ctx->currentPosition > (m_ctx->targetPosition - m_ctx->positionEpsilon);
	u8 low = m_ctx->currentPosition < (m_ctx->targetPosition + m_ctx->positionEpsilon);

	if (high && low)
	{
		m_ctx->MOTOR_ENABLE = MOTOR_OFF;
		m_ctx->MOTOR_MOVING = false;
	}
	if (high)
	{
		m_ctx->MOTOR_DIRECTION = MOTOR_ROTATE_CCW;
	}
	else if (low)
	{
		m_ctx->MOTOR_DIRECTION = MOTOR_ROTATE_CW;
	}

	m_ctx->MOTOR_DIRECTION ^= m_ctx->MOTOR_REVERSAL;

	HAL_GPIO_WritePin(m_ctx->DIR_GPIOx, m_ctx->DIR_GPIO_PIN, m_ctx->MOTOR_DIRECTION);

	return;
}

static void GetInitalCondition()
{
	u32 m0pos = 0, m1pos = 0, m2pos = 0, m3pos = 0;
	u16 newInputs[4] = {0,0,0,0};

	for (int i = 0; i < FILTER_LENGTH; ++i)
	{
		BSP_AdcUpdate(newInputs);

		DF_BoxcarFilter(&BxM0, newInputs[0]);
		DF_BoxcarFilter(&BxM1, newInputs[1]);
		DF_BoxcarFilter(&BxM2, newInputs[2]);
		DF_BoxcarFilter(&BxM3, newInputs[3]);

		osDelay(1);
	}

	m0pos = DF_BoxcarFilter(&BxM0, newInputs[0]);
	m1pos = DF_BoxcarFilter(&BxM1, newInputs[1]);
	m2pos = DF_BoxcarFilter(&BxM2, newInputs[2]);
	m3pos = DF_BoxcarFilter(&BxM3, newInputs[3]);

	m0->currentPosition = (AngleUnit)((float)(m0pos - m0->positionZeroOffset) * 12.5958f);
	m1->currentPosition = ((m1pos - m1->positionZeroOffset) >> 4);
	m2->currentPosition = ((m2pos - m2->positionZeroOffset) >> 4);
	m3->currentPosition = ((m3pos - m3->positionZeroOffset) >> 4);

	return;
}

uint8_t MTR_GetMotorStates()
{
	uint8_t status = 0;

	status |= (m0->MOTOR_MOVING << 0);
	status |= (m1->MOTOR_MOVING << 1);
	status |= (m2->MOTOR_MOVING << 2);
	status |= (m3->MOTOR_MOVING << 3);

	return status;
}

static void MotorOverrunProtection(MotorContext* m_ctx)
{
	if (m_ctx->RUN_TIMER > m_ctx->maxRuntime)
	{
		ADD_FAULT(FAULT_MOTOR_OVERRUN);
		m_ctx->MOTOR_ENABLE = MOTOR_OFF;
	}

	return;
}

static void MotorOverspinProtection(MotorContext* m_ctx)
{
	if ( (m_ctx->currentPosition > m_ctx->maxPosition)
		|| (m_ctx->currentPosition < m_ctx->minPosition))
	{
		m_ctx->MOTOR_ENABLE = MOTOR_OFF;
		m_ctx->PULSE_OUTPUT = 0;
		ADD_FAULT(FAULT_MOTOR_OUT_OF_RANGE);
		HAL_GPIO_WritePin(m_ctx->PULSE_GPIOx, m_ctx->PULSE_GPIO_PIN, m_ctx->PULSE_OUTPUT);
	}
	return;
}

static inline uint16_t InstantMotorSpeed(MotorContext* m_ctx)
{
	// pulses per second * step size
	// step size is approxmiately 1/16 of a degrees (depending on gearing)
	//mCtx->PULSE_FREQ + PULSE_DURATION;




	return 0;
}

static void UpdateMotorPID(MotorContext* m_ctx)
{
	// pulses per second * t + start position
	u32 a = SH_GeneralTimer();
}

static inline uint16_t clamp_s(uint16_t value,  uint16_t min, uint16_t max)
{
	uint16_t clamped;
	if (value < min)
	{
		clamped = min;
	}
	else if (value > max)
	{
		clamped = max;
	}
	else
	{
		clamped = value;
	}
	return clamped;
}

static inline void ResetPin(MotorContext* m_ctx)
{
	// clear controller intergral term
	m_ctx->I_TERM = 0;
	m_ctx->PULSE_COUNTER = 0;
	m_ctx->PULSE_TIMER = 0;

	// enable motor for motion
	m_ctx->MOTOR_ENABLE = MOTOR_ON;
	m_ctx->MOTOR_MOVING = true;
}


