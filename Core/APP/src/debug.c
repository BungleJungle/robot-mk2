/*
 * debug.c
 *
 *  Created on: Dec 24, 2024
 *      Author: Steven
 */
#include "stdio.h"
#include "stdint.h"
#include "main.h"
#include "shared_hardware.h"
#include "system_state.h"

#define LED_FREQ 20

void BDG_DebugTimer()
{
	//
	static uint32_t ticker = 0;
	ticker += 1;

	if (ticker >= LED_FREQ)
	{
		if (signalController.blinkGreenLED == 1)
		{
			SH_GreenLEDToggle();
		}
		if (signalController.blinkRedLED == 1)
		{
			SH_RedLEDToggle();
		}

		ticker = 0;
	}

	return;
}


