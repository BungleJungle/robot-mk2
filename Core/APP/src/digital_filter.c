/*
 * digital_filter.c
 *
 *  Created on: Jan 1, 2025
 *      Author: Steven
 */

#include "digital_filter.h"
#include "types.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"

void DF_BoxcarInit(BxFilterContext* bxCtx)
{
	bxCtx->sum = 0;
	bxCtx->bufferIndex = 0;
	memset(bxCtx->samples, 0, sizeof(u16) * FILTER_LENGTH) ;
	return;
}

u16 DF_BoxcarFilter(BxFilterContext* bxCtx, u16 newestSample)
{
	bxCtx->sum = bxCtx->sum - bxCtx->samples[bxCtx->bufferIndex] + newestSample;
	bxCtx->samples[bxCtx->bufferIndex] = newestSample;

	bxCtx->bufferIndex += 1;
	bxCtx->bufferIndex = bxCtx->bufferIndex % FILTER_LENGTH;

	return bxCtx->sum / FILTER_LENGTH;
}
