/*
 * matrix_math.c
 *
 *  Created on: Jan 1, 2025
 *      Author: Steven
 */


/*
 * matrix_math.h
 *
 *  Created on: May 28, 2024
 *      Author: Steve
 */

#include "matrix_math.h"
#include "math.h"

void m4_cpy(fm4* src0, fm4* dst)
{
	memcpy(dst, src0, sizeof(fm4));
	return;
}

void m4_zero(fm4* src0)
{
	memset(src0, 0, sizeof(fm4));
}

void m4_add(fm4* src0, fm4* src1, fm4* dst)
{
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			dst->m[i][j] = src0->m[i][j] + src1->m[i][j];
		}
	}
	return;
}

void m4_sub(fm4* src0, fm4* src1, fm4* dst)
{
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			dst->m[i][j] = src0->m[i][j] - src1->m[i][j];
		}
	}
	return;
}

void m4_mul(fm4* src0, fm4* src1, fm4* dst)
{
	for (int i = 0; i < 4; ++i)
	{
		dst->m[i][0] = src0->m[i][0] * src1->m[0][0] + src0->m[i][1] * src1->m[1][0] + src0->m[i][2] * src1->m[2][0] + src0->m[i][3] * src1->m[3][0];
		dst->m[i][1] = src0->m[i][0] * src1->m[0][1] + src0->m[i][1] * src1->m[1][1] + src0->m[i][2] * src1->m[2][1] + src0->m[i][3] * src1->m[3][1];
		dst->m[i][2] = src0->m[i][0] * src1->m[0][2] + src0->m[i][1] * src1->m[1][2] + src0->m[i][2] * src1->m[2][2] + src0->m[i][3] * src1->m[3][2];
		dst->m[i][3] = src0->m[i][0] * src1->m[0][3] + src0->m[i][1] * src1->m[1][3] + src0->m[i][2] * src1->m[2][3] + src0->m[i][3] * src1->m[3][3];
	}
	return;
}

void m4_rot(fm4* dst, float t, float a, float d, float r)
{
	dst->m[0][0] = cos(t);
	dst->m[0][1] = -sin(t) * cos(a);
	dst->m[0][2] = sin(t) * sin(a);
	dst->m[0][3] = r * cos(t);

	dst->m[1][0] = sin(t);
	dst->m[1][1] = cos(t) * cos(a);
	dst->m[1][2] = -cos(t) * sin(a);
	dst->m[1][3] = r*sin(t);

	dst->m[2][0] = 0;
	dst->m[2][1] = sin(a);
	dst->m[2][2] = cos(a);
	dst->m[2][3] = d;

	dst->m[3][0] = 0;
	dst->m[3][1] = 0;
	dst->m[3][2] = 0;
	dst->m[3][3] = 1;
	return;
}

void m4_tnspos(fm4* src, fm4* dst)
{
	//
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			dst->m[i][j] = src->m[j][i];
		}
	}
	return;
}


void m3_cpy(fm3* src0, fm3* dst)
{
	memcpy(dst, src0, sizeof(fm3));
	return;
}

void m3_zero(fm3* src0)
{
	memset(src0, 0, sizeof(fm3));
	return;
}

void m3_rot(fm3* dst, float x, float y, float z)
{
	// alpha = z
	// beta = y
	// gamma = x
	dst->m[0][0] = cos(x) * cos(y);
	dst->m[0][1] = cos(z) * sin(y) * cos(x) - cos(z) * sin(y);
	dst->m[0][2] = cos(z) * sin(y) * cos(x) + sin(z) * sin(x);

	dst->m[1][0] = sin(z) * cos(y);
	dst->m[1][1] = sin(z) * sin(y) * sin(x) + cos(z) * cos(x);
	dst->m[1][2] = sin(z) * sin(y) * cos(x) - cos(z) * sin(x);

	dst->m[2][0] = -sin(y);
	dst->m[2][1] = cos(y) * sin(x);
	dst->m[2][2] = cos(y) * cos(x);
	return;
}


// math operations
void m3_add(fm3* src0, fm3* src1, fm3* dst)
{
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			dst->m[i][j] = src0->m[i][j] + src1->m[i][j];
		}
	}
	return;
}

void m3_sub(fm3* src0, fm3* src1, fm3* dst)
{
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			dst->m[i][j] = src0->m[i][j] - src1->m[i][j];
		}
	}
	return;
}

void m3_mul(fm3* src0, fm3* src1, fm3* dst)
{
	for (int i = 0; i < 3; ++i)
	{
		dst->m[i][0] = src0->m[i][0] * src1->m[0][0] + src0->m[i][1] * src1->m[1][0] + src0->m[i][2] * src1->m[2][0];
		dst->m[i][1] = src0->m[i][0] * src1->m[0][1] + src0->m[i][1] * src1->m[1][1] + src0->m[i][2] * src1->m[2][1];
		dst->m[i][2] = src0->m[i][0] * src1->m[0][2] + src0->m[i][1] * src1->m[1][2] + src0->m[i][2] * src1->m[2][2];
	}
	return;
}

// advanced matrix operations
void m3_tnspos(fm3* src, fm3* dst)
{
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			dst->m[i][j] = src->m[j][i];
		}
	}
	return;
}

