/*
 * kinematics.h
 *
 *  Created on: Jan 1, 2025
 *      Author: Steven
 */

#ifndef APP_INC_KINEMATICS_H_
#define APP_INC_KINEMATICS_H_

#include "matrix_math.h"

#define NUMBER_OF_FRAMES 6

typedef struct DHParams
{
	// t - theta in degrees
	// a - alpha in degrees
	// d - distance in mm
	// r - distance in mm
	float t; float a; float d; float r;
}DHParams;


extern DHParams dhTable[NUMBER_OF_FRAMES];
extern fm4 		endEffector;

void KM_Init();
void KM_KinematicsCyclic();
void KM_ForwardKinematics();
void KM_InverseKinematics(DHParams* dhTable, fm4* destination);


#endif /* APP_INC_KINEMATICS_H_ */

