/*
 * digital_filter.h
 *
 *  Created on: Jan 1, 2025
 *      Author: Steven
 */

#ifndef APP_INC_DIGITAL_FILTER_H_
#define APP_INC_DIGITAL_FILTER_H_

#include "stdlib.h"
#include "stdint.h"
#include "types.h"

#define FILTER_LENGTH	16

typedef struct
{
	u16 samples[FILTER_LENGTH];
	u32 sum				: 24;
	u8	bufferIndex		: 8;
}BxFilterContext;

void DF_BoxcarInit(BxFilterContext* bxCtx);
u16 DF_BoxcarFilter(BxFilterContext* bxCtx, u16 newestSample);

#endif /* APP_INC_DIGITAL_FILTER_H_ */
