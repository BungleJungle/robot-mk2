/*
 * matrix_math.h
 *
 *  Created on: Jan 1, 2025
 *      Author: Steven
 */

#ifndef APP_INC_MATRIX_MATH_H_
#define APP_INC_MATRIX_MATH_H_

#include "stdio.h"

typedef struct fm4 fm4;
struct fm4
{
	float m[4][4];
};

typedef struct fm3 fm3;
struct fm3
{
	float m[3][3];
};


/*
 * 4 X 4 Matrix tools
 *
 */
void m4_cpy(fm4* src0, fm4* dst);
void m4_zero(fm4* src0);

// where:
// t is the variable of rotation
// a is the rotation of the x axis
// d is the displacement along z
// r is the displacement along x
void m4_rot(fm4* dst, float t, float a, float d, float r);

// math operations
void m4_add(fm4* src0, fm4* src1, fm4* dst);
void m4_sub(fm4* src0, fm4* src1, fm4* dst);
void m4_mul(fm4* src0, fm4* src1, fm4* dst);

// advanced matrix operations
void m4_tnspos(fm4* src, fm4* dst);


/*
 * 3 X 3 Matrix tools
 *
 */
void m3_cpy(fm3* src0, fm3* dst);
void m3_zero(fm3* src0);

// where:
// t is the variable of rotation
// a is the rotation of the x axis
// d is the displacement along z
// r is the displacement along x
void m3_rot(fm3* dst, float rotX, float rotY, float rotZ);

// math operations
void m3_add(fm3* src0, fm3* src1, fm3* dst);
void m3_sub(fm3* src0, fm3* src1, fm3* dst);
void m3_mul(fm3* src0, fm3* src1, fm3* dst);

// advanced matrix operations
void m3_tnspos(fm3* src, fm3* dst);


#endif /* APP_INC_MATRIX_MATH_H_ */
