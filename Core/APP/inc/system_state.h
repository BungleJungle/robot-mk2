/*
 * system_state.h
 *
 *  Created on: Dec 23, 2024
 *      Author: Steven
 */

#ifndef APP_INC_SYSTEM_STATE_H_
#define APP_INC_SYSTEM_STATE_H_

#include "stdio.h"
#include "stdio.h"
#include "stdint.h"
#include "types.h"

#define NUMBER_OF_WARNING	4
#define NUMBER_OF_FAULTS	4

typedef struct SignalController
{
	u8 systemState;
	u8 warningFlags[NUMBER_OF_WARNING];
	u8 warningIndexer;
	u8 faultFlags[NUMBER_OF_FAULTS];
	u8 faultIndexer;


	u8 blinkRedLED : 1;
	u8 blinkGreenLED : 1;
	u8 MTR_INIT : 1;
	u8 initalPowerUp : 1;
}SignalController;

typedef enum
{
	STATE_INIT,
	STATE_IDLE,
	STATE_HOMING,
	STATE_PLANNING,
	STATE_MOVING,
	STATE_AT_TARGET,
	STATE_RETURNING,
	STATE_REST,
	STATE_FAULTED,

	NUMBER_OF_STATES
};

typedef enum
{
	W_NO_COMMS,
	W_NO_SOLUTION,
	W_NO_TRAJECTORY,
	W_INVALID_SPEED_REQUEST,
	W_INVALID_POSITION_REQUEST,
	W_INVALID_CHANNEL_REQUEST,

	WARNING_COUNT
};

typedef enum
{
	FAULT_ADC0_IMPLAUSIBLE,
	FAULT_ADC1_IMPLAUSIBLE,
	FAULT_ADC2_IMPLAUSIBLE,
	FAULT_ADC3_IMPLAUSIBLE,
	FAULT_ADC4_IMPLAUSIBLE,
	FAULT_ADC5_IMPLAUSIBLE,

	FAULT_M0_OVERSPIN,
	FAULT_M1_OVERSPIN,
	FAULT_M2_OVERSPIN,
	FAULT_M3_OVERSPIN,
	FAULT_M4_OVERSPIN,
	FAULT_M5_OVERSPIN,

	FAULT_M0_STALL,
	FAULT_M1_STALL,
	FAULT_M2_STALL,
	FAULT_M3_STALL,
	FAULT_M4_STALL,
	FAULT_M5_STALL,

	FAULT_MOTOR_OVERRUN,
	FAULT_MOTOR_OUT_OF_RANGE,

	FAULT_COUNT
};

extern SignalController signalController;

ReturnType ST_Init();
ReturnType ST_Cyclic();

static inline void ADD_WARNING(u16 w_id)
{
	signalController.warningFlags[signalController.warningIndexer] = w_id;
	signalController.warningIndexer = (signalController.warningIndexer + 1) % NUMBER_OF_WARNING;
}

static inline void ADD_FAULT(u16 f_id)
{
	signalController.faultFlags[signalController.faultIndexer] = f_id;
	signalController.faultIndexer = (signalController.faultIndexer + 1) % NUMBER_OF_FAULTS;
}

#endif /* APP_INC_SYSTEM_STATE_H_ */
