/*
 * motor_controller.h
 *
 *  Created on: Dec 23, 2024
 *      Author: Steven
 */

#ifndef APP_INC_MOTOR_CONTROLLER_H_
#define APP_INC_MOTOR_CONTROLLER_H_

#define NUMBER_OF_COEFF	4

#include "stdlib.h"
#include "stdint.h"
#include "types.h"
#include "gpio.h"

typedef struct MotorContext
{
	// configurations port
	GPIO_TypeDef*	DIR_GPIOx;
	GPIO_TypeDef*	PULSE_GPIOx;

	// timers only refernce these
	u32 			maxRuntime;		// time in mS
	u32 			RUN_TIMER;		// time in mS
	u32				START_TIME;

	// configurations pins
	uint16_t 		DIR_GPIO_PIN;
	uint16_t 		PULSE_GPIO_PIN;

	// trajectory planning
	float 			stepSize;	// step size in degrees (stepper motor * gear reduction)
	float 			coefficents[NUMBER_OF_COEFF];

	// position data
	AngleUnit 		positionZeroOffset;	// start position for watchdawg
	AngleUnit 		positionLimit;		// end position for watchdawg
	u16				maxSpeed;			// units in 1/16 of a degree per second
	u16 			minSpeed;			// units in 1/16 of a degree per second
	AngleUnit		minPosition;		//
	AngleUnit		maxPosition;		// 

	// movement
	AngleUnit 			targetPosition;		// target position in 1/16 of a degree
	volatile AngleUnit 	currentPosition;	// actual motor position in 1/16 of a degree
	AngleUnit 			positionEpsilon;	// tolerance for motor position target
	AngleUnit  			homePosition;


	u8				coefficentCount;
	u8  			polynomialStepCount;

	//D PI controller
	s16				I_TERM;				// I-term, dynamic time step adjument during movemnet
	s16 			START_POSITION;		//

	// controller interface (DO NOT TOUCH OUTSIDE ISR!!!)
	u16 			PULSE_TIMER;
	u16 			PULSE_FREQ;
	u16 			PULSE_COUNTER;
	s8				DIR_DEBOUNCE;
	u8 				MOTOR_ENABLE 	: 1;		// enable motor operation
	u8 				MOTOR_DIRECTION : 1;		// motor direction pin
	u8 				MOTOR_ALARM		: 1;		// input from the motor controller
	u8 				MOTOR_REVERSAL	: 1;		// reverse the forward direction
	u8				PULSE_OUTPUT	: 1;		//
	u8				MOTOR_MOVING	: 1;

}MotorContext;

extern MotorContext* const m0;
extern MotorContext* const m1;
extern MotorContext* const m2;
extern MotorContext* const m3;

ReturnType MTR_Init();
void MTR_CheckPositionISR();
void MTR_PulseISR();
void MTR_MoveMotor(uint8_t motor_id, float position, float time);
void MTR_MoveMotorNonLinear(uint8_t motor_id, float position, float time);
void MTR_Cylcic();
u8 MTR_GetMotorStates();
void MTR_StartPulse();
void MTR_EndPulse();

#endif /* APP_INC_MOTOR_CONTROLLER_H_ */
