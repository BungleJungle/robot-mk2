/*
 * debug.h
 *
 *  Created on: Dec 24, 2024
 *      Author: Steven
 */

#ifndef APP_INC_DEBUG_H_
#define APP_INC_DEBUG_H_

#include "stdlib.h"
#include "stdint.h"
#include "types.h"

void BDG_DebugTimer();

#endif /* APP_INC_DEBUG_H_ */
