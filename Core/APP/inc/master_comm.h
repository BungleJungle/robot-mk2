/*
 * master_comm.h
 *
 *  Created on: Dec 27, 2024
 *      Author: Steven
 */

#ifndef APP_INC_MASTER_COMM_H_
#define APP_INC_MASTER_COMM_H_

#include "types.h"

ReturnType MC_GetMessage();

#endif /* APP_INC_MASTER_COMM_H_ */
