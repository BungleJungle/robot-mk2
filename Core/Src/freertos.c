/* USER CODE BEGIN Header */
/**
	******************************************************************************
	* File Name          : freertos.c
	* Description        : Code for freertos applications
	******************************************************************************
	* @attention
	*
	* Copyright (c) 2024 STMicroelectronics.
	* All rights reserved.
	*
	* This software is licensed under terms that can be found in the LICENSE file
	* in the root directory of this software component.
	* If no LICENSE file comes with this software, it is provided AS-IS.
	*
	******************************************************************************
	*/
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "tim.h"
#include "adc_manager.h"
#include "debug.h"
#include "ethernet_manager.h"
#include "master_comm.h"
#include "kinematics.h"
#include "motor_controller.h"
#include "system_state.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
typedef StaticTask_t osStaticThreadDef_t;
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
#define TICK_PER_CYCLE_20HZ 	50
#define TICK_PER_CYCLE_50HZ 	20
#define TICK_PER_CYCLE_100HZ 	10
#define TICK_ETHERNET_LOOP		500
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for task50ms */
osThreadId_t task50msHandle;
const osThreadAttr_t task50ms_attributes = {
  .name = "task50ms",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityBelowNormal,
};
/* Definitions for task20ms */
osThreadId_t task20msHandle;
const osThreadAttr_t task20ms_attributes = {
  .name = "task20ms",
  .stack_size = 512 * 4,
  .priority = (osPriority_t) osPriorityRealtime,
};
/* Definitions for stateMachine */
osThreadId_t stateMachineHandle;
const osThreadAttr_t stateMachine_attributes = {
  .name = "stateMachine",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for ethernetControl */
osThreadId_t ethernetControlHandle;
uint32_t ethernet_thread[ 1024 ];
osStaticThreadDef_t ethernetControlBlock;
const osThreadAttr_t ethernetControl_attributes = {
  .name = "ethernetControl",
  .cb_mem = &ethernetControlBlock,
  .cb_size = sizeof(ethernetControlBlock),
  .stack_mem = &ethernet_thread[0],
  .stack_size = sizeof(ethernet_thread),
  .priority = (osPriority_t) osPriorityBelowNormal1,
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void startTask50ms(void *argument);
void StartTask20ms(void *argument);
void stateMachineTask(void *argument);
void ethernetControlLoop(void *argument);

extern void MX_LWIP_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

	HAL_TIM_Base_Start_IT(&htim14);

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of task50ms */
  task50msHandle = osThreadNew(startTask50ms, NULL, &task50ms_attributes);

  /* creation of task20ms */
  task20msHandle = osThreadNew(StartTask20ms, NULL, &task20ms_attributes);

  /* creation of stateMachine */
  stateMachineHandle = osThreadNew(stateMachineTask, NULL, &stateMachine_attributes);

  /* creation of ethernetControl */
  ethernetControlHandle = osThreadNew(ethernetControlLoop, NULL, &ethernetControl_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
	/* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_startTask50ms */
/**
	* @brief  Function implementing the task50ms thread.
	* @param  argument: Not used
	* @retval None
	*/
/* USER CODE END Header_startTask50ms */
void startTask50ms(void *argument)
{
  /* init code for LWIP */

  /* USER CODE BEGIN startTask50ms */
	/* Infinite loop */
	for(;;)
	{
		uint32_t cycleTicks = osKernelGetTickCount();

		BDG_DebugTimer();

		osStatus_t ligma = osDelayUntil(cycleTicks + TICK_PER_CYCLE_20HZ);
	}
  /* USER CODE END startTask50ms */
}

/* USER CODE BEGIN Header_StartTask20ms */
/**
* @brief Function implementing the task20ms thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask20ms */
void StartTask20ms(void *argument)
{
  /* USER CODE BEGIN StartTask20ms */
	uint16_t timer = 0;

	/* Infinite loop */
	for(;;)
	{
		uint32_t cycleTicks = osKernelGetTickCount();

		//htim14.Instance->CNT = 0;

		KM_KinematicsCyclic();

		MTR_Cylcic();

		//timer = htim14.Instance->CNT;
		//timer = timer;

		osStatus_t ligma = osDelayUntil(cycleTicks + TICK_PER_CYCLE_50HZ);
	}
  /* USER CODE END StartTask20ms */
}

/* USER CODE BEGIN Header_stateMachineTask */
/**
* @brief Function implementing the stateMachine thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_stateMachineTask */
void stateMachineTask(void *argument)
{
  /* USER CODE BEGIN stateMachineTask */

  /* Infinite loop */
	for(;;)
	{
		uint32_t cycleTicks = osKernelGetTickCount();

		ST_Cyclic();

		osStatus_t ligma = osDelayUntil(cycleTicks + TICK_PER_CYCLE_100HZ);
	}
  /* USER CODE END stateMachineTask */
}

/* USER CODE BEGIN Header_ethernetControlLoop */
/**
* @brief Function implementing the ethernetControl thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_ethernetControlLoop */
void ethernetControlLoop(void *argument)
{
  /* USER CODE BEGIN ethernetControlLoop */
  /* Infinite loop */
	for(;;)
	{
		uint32_t cycleTicks = osKernelGetTickCount();

		MC_GetMessage();

		osStatus_t ligma = osDelayUntil(cycleTicks + TICK_ETHERNET_LOOP);
	}
  /* USER CODE END ethernetControlLoop */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

