/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
#define MOTOR_0				0
#define MOTOR_1				1
#define MOTOR_2				2
#define MOTOR_3				3
#define MOTOR_4				4
#define MOTOR_5				5

typedef enum
{
	STEPPER_M0,
	STEPPER_M1,
	STEPPER_M2,
	STEPPER_M3,
	SERVO_M4,
	SERVO_M5,

	NUMBER_OF_MOTORS
};


//#define NUMBER_OF_MOTORS 	3
#define CACHE_LINE_SIZE 	32
/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
#define CACHE_LINE_SIZE	32
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MOTOR_2_PULSE_Pin GPIO_PIN_0
#define MOTOR_2_PULSE_GPIO_Port GPIOF
#define MOTOR_2_DIRECTION_Pin GPIO_PIN_1
#define MOTOR_2_DIRECTION_GPIO_Port GPIOF
#define MOTOR_0_POSITION_Pin GPIO_PIN_0
#define MOTOR_0_POSITION_GPIO_Port GPIOC
#define MOTOR_2_POSITION_Pin GPIO_PIN_0
#define MOTOR_2_POSITION_GPIO_Port GPIOA
#define MOTOR_1_POSITION_Pin GPIO_PIN_4
#define MOTOR_1_POSITION_GPIO_Port GPIOA
#define GREEN_LED_Pin GPIO_PIN_0
#define GREEN_LED_GPIO_Port GPIOB
#define MOTOR_3_POSITION_Pin GPIO_PIN_1
#define MOTOR_3_POSITION_GPIO_Port GPIOB
#define RED_LED_Pin GPIO_PIN_14
#define RED_LED_GPIO_Port GPIOB
#define MOTOR_1_PULSE_Pin GPIO_PIN_9
#define MOTOR_1_PULSE_GPIO_Port GPIOD
#define MOTOR_3_PULSE_Pin GPIO_PIN_10
#define MOTOR_3_PULSE_GPIO_Port GPIOC
#define MOTOR_3_DIRECTION_Pin GPIO_PIN_12
#define MOTOR_3_DIRECTION_GPIO_Port GPIOC
#define MOTOR_0_PULSE_Pin GPIO_PIN_0
#define MOTOR_0_PULSE_GPIO_Port GPIOD
#define MOTOR_0_DIRECTION_Pin GPIO_PIN_1
#define MOTOR_0_DIRECTION_GPIO_Port GPIOD
#define MOTOR_1_DIRECTION_Pin GPIO_PIN_12
#define MOTOR_1_DIRECTION_GPIO_Port GPIOG
#define AMBER_LED_Pin GPIO_PIN_1
#define AMBER_LED_GPIO_Port GPIOE

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
