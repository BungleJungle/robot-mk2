/*
 * types.h
 *
 *  Created on: Dec 23, 2024
 *      Author: Steven
 */

#ifndef INC_TYPES_H_
#define INC_TYPES_H_

#include "stdlib.h"
#include "stdint.h"

#define IDEG2ANGLE(X)  (X << 4)
#define FDEG2ANGLE(X)  (((uint16_t)X) << 4)

typedef enum
{
	OK,
	BUSY,
	INVALID_ARG,
	ERROR_T
} ReturnType;

typedef uint8_t 	u8;
typedef int8_t		s8;
typedef uint16_t	u16;
typedef int16_t		s16;
typedef uint32_t	u32;
typedef int32_t		s32;

typedef int16_t		AngleUnit;

#endif /* INC_TYPES_H_ */
