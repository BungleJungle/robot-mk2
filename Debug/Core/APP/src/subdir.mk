################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (12.3.rel1)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/APP/src/debug.c \
../Core/APP/src/digital_filter.c \
../Core/APP/src/kinematics.c \
../Core/APP/src/master_comm.c \
../Core/APP/src/matrix_math.c \
../Core/APP/src/motor_controller.c \
../Core/APP/src/system_state.c 

OBJS += \
./Core/APP/src/debug.o \
./Core/APP/src/digital_filter.o \
./Core/APP/src/kinematics.o \
./Core/APP/src/master_comm.o \
./Core/APP/src/matrix_math.o \
./Core/APP/src/motor_controller.o \
./Core/APP/src/system_state.o 

C_DEPS += \
./Core/APP/src/debug.d \
./Core/APP/src/digital_filter.d \
./Core/APP/src/kinematics.d \
./Core/APP/src/master_comm.d \
./Core/APP/src/matrix_math.d \
./Core/APP/src/motor_controller.d \
./Core/APP/src/system_state.d 


# Each subdirectory must supply rules for building sources it contributes
Core/APP/src/%.o Core/APP/src/%.su Core/APP/src/%.cyclo: ../Core/APP/src/%.c Core/APP/src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu99 -g3 -DDEBUG -DUSE_PWR_LDO_SUPPLY -DUSE_HAL_DRIVER -DSTM32H723xx -c -I../Core/Inc -I../Core/BSP/inc -I../Core/APP/inc -I../Drivers/STM32H7xx_HAL_Driver/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../Drivers/CMSIS/Include -I../LWIP/App -I../LWIP/Target -I../Middlewares/Third_Party/LwIP/src/include -I../Middlewares/Third_Party/LwIP/system -I../Drivers/BSP/Components/lan8742 -I../Middlewares/Third_Party/LwIP/src/include/netif/ppp -I../Middlewares/Third_Party/LwIP/src/include/lwip -I../Middlewares/Third_Party/LwIP/src/include/lwip/apps -I../Middlewares/Third_Party/LwIP/src/include/lwip/priv -I../Middlewares/Third_Party/LwIP/src/include/lwip/prot -I../Middlewares/Third_Party/LwIP/src/include/netif -I../Middlewares/Third_Party/LwIP/src/include/compat/posix -I../Middlewares/Third_Party/LwIP/src/include/compat/posix/arpa -I../Middlewares/Third_Party/LwIP/src/include/compat/posix/net -I../Middlewares/Third_Party/LwIP/src/include/compat/posix/sys -I../Middlewares/Third_Party/LwIP/src/include/compat/stdc -I../Middlewares/Third_Party/LwIP/system/arch -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-APP-2f-src

clean-Core-2f-APP-2f-src:
	-$(RM) ./Core/APP/src/debug.cyclo ./Core/APP/src/debug.d ./Core/APP/src/debug.o ./Core/APP/src/debug.su ./Core/APP/src/digital_filter.cyclo ./Core/APP/src/digital_filter.d ./Core/APP/src/digital_filter.o ./Core/APP/src/digital_filter.su ./Core/APP/src/kinematics.cyclo ./Core/APP/src/kinematics.d ./Core/APP/src/kinematics.o ./Core/APP/src/kinematics.su ./Core/APP/src/master_comm.cyclo ./Core/APP/src/master_comm.d ./Core/APP/src/master_comm.o ./Core/APP/src/master_comm.su ./Core/APP/src/matrix_math.cyclo ./Core/APP/src/matrix_math.d ./Core/APP/src/matrix_math.o ./Core/APP/src/matrix_math.su ./Core/APP/src/motor_controller.cyclo ./Core/APP/src/motor_controller.d ./Core/APP/src/motor_controller.o ./Core/APP/src/motor_controller.su ./Core/APP/src/system_state.cyclo ./Core/APP/src/system_state.d ./Core/APP/src/system_state.o ./Core/APP/src/system_state.su

.PHONY: clean-Core-2f-APP-2f-src

